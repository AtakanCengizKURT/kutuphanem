//
//  ViewController.swift
//  Kütüphanem
//
//  Created by Atakan Cengiz KURT on 31.05.2017.
//  Copyright © 2017 Atakan Cengiz KURT. All rights reserved.
//

import UIKit
import MapKit
import CoreLocation
import SystemConfiguration

class ViewController: UIViewController, CLLocationManagerDelegate {
    @IBOutlet weak var map: MKMapView!
   
    @IBOutlet weak var label: UILabel!

    let manager = CLLocationManager()
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        //Yer belirleme
        let location = locations[0]
        let span:MKCoordinateSpan = MKCoordinateSpanMake(0.01, 0.01)
        let myLocation:CLLocationCoordinate2D = CLLocationCoordinate2DMake(location.coordinate.latitude, location.coordinate.longitude)
        let region:MKCoordinateRegion = MKCoordinateRegionMake(myLocation, span)
        map.setRegion(region, animated: true)
        self.map.showsUserLocation = true
        //Yer belirleme
        //Adresi belirleme
        CLGeocoder().reverseGeocodeLocation(location) { (placemark, error) in
            if error != nil
            {
                print("Konum bilgisi alınamıyor")
            }
            else
            {
                if let place = placemark?[0]
                {
                    if let checker = place.subThoroughfare
                    {
                        self.label.text = "\(place.subThoroughfare!) \n\(place.thoroughfare!) \n\(place.country!)"
                    }
                }
            }
        }
    }
    
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        if isInternetAvailable() == false
        {
            alertController(title: "İNTERNET HATASI", message: "İnternet bağlantınız yok!")
        }
        
        
        // Do any additional setup after loading the view, typically from a nib.
        manager.delegate = self
        manager.desiredAccuracy = kCLLocationAccuracyBest
        manager.requestWhenInUseAuthorization()
        manager.startUpdatingLocation()
        
        
        //konum ekleme
        let span:MKCoordinateSpan = MKCoordinateSpanMake(0.1, 0.1)
        let location:CLLocationCoordinate2D = CLLocationCoordinate2DMake(41.039126, 28.989589)
        let region:MKCoordinateRegion = MKCoordinateRegionMake(location, span)
        map.setRegion(region, animated: true)
        //konum ekleme
        
        //konumlar
        struct Location {
            let title: String
            let subtitle: String
            let latitude: Double
            let longitude: Double
        }
        
        let locations = [
            Location(title: "Atatürk Kitaplığı", subtitle:"7/24 Açık",  latitude: 41.039167, longitude: 28.989568),
            Location(title: "Los Angeles, CA", subtitle:" ",latitude: 34.052238, longitude: -118.243344),
            Location(title: "Chicago, IL",    subtitle:" ", latitude: 41.883229, longitude: -87.632398)
        ]
        
        for location in locations {
            let annotation = MKPointAnnotation()
            annotation.title = location.title
            annotation.subtitle = location.subtitle
            annotation.coordinate = CLLocationCoordinate2D(latitude: location.latitude, longitude: location.longitude)
            map.addAnnotation(annotation)
        }
        //konumlar
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


    //İnternet kontrolü
    func isInternetAvailable() -> Bool
    {
        var zeroAddress = sockaddr_in()
        zeroAddress.sin_len = UInt8(MemoryLayout.size(ofValue: zeroAddress))
        zeroAddress.sin_family = sa_family_t(AF_INET)
        
        let defaultRouteReachability = withUnsafePointer(to: &zeroAddress) {
            $0.withMemoryRebound(to: sockaddr.self, capacity: 1) {zeroSockAddress in
                SCNetworkReachabilityCreateWithAddress(nil, zeroSockAddress)
            }
        }
        
        var flags = SCNetworkReachabilityFlags()
        if !SCNetworkReachabilityGetFlags(defaultRouteReachability!, &flags) {
            return false
        }
        let isReachable = (flags.rawValue & UInt32(kSCNetworkFlagsReachable)) != 0
        let needsConnection = (flags.rawValue & UInt32(kSCNetworkFlagsConnectionRequired)) != 0
        return (isReachable && !needsConnection)
    }
    //internet kontrolü
    
    //alert
    func alertController(title: String, message: String)
    {
        let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertControllerStyle.alert)
        self.present(alert, animated: true, completion: nil)
        
        let btnCancel = UIAlertAction(title: "Tamam", style: UIAlertActionStyle.cancel) { (ACTION) in
            
        }
        alert.addAction(btnCancel)
    }
    //alert
}

